package net.synergyserver.synergycreative.listeners;

import net.synergyserver.synergycore.configs.PluginConfig;
import net.synergyserver.synergycore.profiles.MinecraftProfile;
import net.synergyserver.synergycore.utils.ItemUtil;
import net.synergyserver.synergycore.utils.PlayerUtil;
import net.synergyserver.synergycreative.SynergyCreative;
import net.synergyserver.synergycreative.settings.CreativeToggleSetting;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerChangedWorldEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

/**
 * Listens to player movement events.
 */
public class PlayerMoveListener implements Listener {

    @EventHandler(priority = EventPriority.LOWEST, ignoreCancelled = true)
    public void onPlayerSwitchWorlds(PlayerChangedWorldEvent event) {
        Player player = event.getPlayer();

        // Ignore the event if it doesn't take place in this plugin's world group
        if (!SynergyCreative.getWorldGroup().contains(player.getWorld().getName())) {
            return;
        }

        MinecraftProfile mcp = PlayerUtil.getProfile(player);

        // Delay it shortly because Bukkit is stupid
        Bukkit.getScheduler().runTaskLater(SynergyCreative.getPlugin(), new Runnable() {
            @Override
            public void run() {
                // If night_vision is set to true then give them night vision
                if (CreativeToggleSetting.NIGHT_VISION.getValue(mcp)) {
                    // Fix for multiverse being stupid
                    if (player.hasPotionEffect(PotionEffectType.NIGHT_VISION)) {
                        player.removePotionEffect(PotionEffectType.NIGHT_VISION);
                    }
                    player.addPotionEffect(new PotionEffect(PotionEffectType.NIGHT_VISION, Integer.MAX_VALUE, 0, false, false));
                } else {
                    player.removePotionEffect(PotionEffectType.NIGHT_VISION);
                }
            }
        }, 1L);
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onPlayerMove(PlayerMoveEvent event) {
        Player player = event.getPlayer();

        // Ignore the event if it doesn't take place in this plugin's world group
        if (!SynergyCreative.getWorldGroup().contains(player.getWorld().getName())) {
            return;
        }

        MinecraftProfile mcp = PlayerUtil.getProfile(player);

        // If noclip is set to true then change their gamemode accordingly
        if (CreativeToggleSetting.NOCLIP.getValue(mcp)) {
            Location location = player.getLocation();
            GameMode currentGamemode = player.getGameMode();
            double radius = PluginConfig.getConfig(SynergyCreative.getPlugin()).getDouble("noclip_radius");

            // Loop through all blocks in the radius to try to find a non-air block
            boolean foundBlock = false;
            loop: for (double i = location.getX() - radius; i <= location.getX() + radius; i++) {
                // Add one to Y because players are two blocks tall
                for (double j = location.getY() - radius; j <= location.getY() + radius + 1; j++) {
                    for (double k = location.getZ() - radius; k <= location.getZ() + radius; k++) {
                        Location blockLocation = new Location(location.getWorld(), i, j, k);
                        // Continue until a non-air block is found
                        if (ItemUtil.isAir(blockLocation.getBlock().getType())) {
                            continue;
                        }
                        foundBlock = true;
                        break loop;
                    }
                }
            }

            // Change the player's gamemode, if applicable
            if (foundBlock && !currentGamemode.equals(GameMode.SPECTATOR)) {
                player.setGameMode(GameMode.SPECTATOR);
            } else if (!foundBlock && !currentGamemode.equals(GameMode.CREATIVE)) {
                player.setGameMode(GameMode.CREATIVE);
                player.setFlying(true);
            }
        }
    }

}
