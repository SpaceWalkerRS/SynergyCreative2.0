package net.synergyserver.synergycreative.inputs;

import com.boydti.fawe.object.RunnableVal;
import com.boydti.fawe.util.TaskManager;
import com.sk89q.worldedit.WorldEditException;
import com.sk89q.worldedit.extent.Extent;
import com.sk89q.worldedit.extent.logging.AbstractLoggingExtent;
import com.sk89q.worldedit.math.BlockVector3;
import com.sk89q.worldedit.world.block.BlockStateHolder;
import net.synergyserver.synergycore.configs.Message;
import net.synergyserver.synergycreative.utils.PlotUtil;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Player;

/**
 * Prevents WorldEdit operations from modifying registered >code>Inputs</code>.
 */
public class InputMaskingExtent extends AbstractLoggingExtent {

    private World world;
    private Player player;

    public InputMaskingExtent(Extent extent, World world, Player player) {
        super(extent);
        this.world = world;
        this.player = player;
    }

    @Override
    public void onBlockChange(BlockVector3 location, BlockStateHolder block) throws WorldEditException {
        TaskManager.IMP.syncWhenFree(new RunnableVal<Object>() {
            @Override
            public void run(Object value) {
                // Create a Bukkit location
                Location loc = new Location(world, location.getBlockX(), location.getBlockY(), location.getBlockZ());

                // Ignore this block if it isn't an Input
                InputManager im = InputManager.getInstance();
                Input input = im.getInput(loc);
                if (input == null) {
                    return;
                }

                // If the player doesn't have permission to build at the location then assume that the edit will fail and ignore this block
                if (!PlotUtil.canBuild(player, loc)) {
                    return;
                }

                // Ignore if the block in the world still matches the input
                if (world.getBlockAt(loc).getType().equals(input.getBlockType())) {
                    return;

                }

                // Delete the input from the database
                im.deleteInput(input);

                // Warn the player that they removed an input
                boolean bypassing = player.hasPermission("syn.input.remove.bypass") && !PlotUtil.canBuild(player, input.getLocation());
                if (bypassing) {
                    player.sendMessage(Message.format("events.input_removal.warning.worldedit_removed_bypassed_ownership", loc.getBlockX(), loc.getBlockY(), loc.getBlockZ()));
                } else {
                    player.sendMessage(Message.format("events.input_removal.warning.worldedit_removed", loc.getBlockX(), loc.getBlockY(), loc.getBlockZ()));
                }
            }
        });
    }
}
