package net.synergyserver.synergycreative.commands;

import net.synergyserver.synergycore.commands.CommandDeclaration;
import net.synergyserver.synergycore.commands.CommandFlags;
import net.synergyserver.synergycore.commands.SubCommand;
import net.synergyserver.synergycore.configs.Message;
import org.bukkit.Material;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Firework;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.meta.FireworkMeta;

@CommandDeclaration(
        commandName = "launch",
        permission = "syn.firework.launch",
        usage = "/firework launch",
        description = "Launches a set of the fireworks in a player's hand",
        parentCommandName = "firework"
)

public class FireworkLaunchCommand extends SubCommand {

    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        Player player = (Player) sender;
        PlayerInventory inventory = player.getInventory();

        ItemStack fireworkItem = inventory.getItemInMainHand();
        FireworkMeta fireworkMeta = (FireworkMeta) fireworkItem.getItemMeta();

        if(!fireworkItem.getType().equals(Material.FIREWORK_ROCKET)){
            player.sendMessage(Message.get("commands.firework.error.no_firework"));
            return false;
        }

        Firework firework = (Firework) player.getWorld().spawnEntity(player.getLocation(), EntityType.FIREWORK);
        firework.setFireworkMeta(fireworkMeta);

        player.sendMessage(Message.get("commands.firework.launch.info.launched_firework"));

        return true;
    }
}
