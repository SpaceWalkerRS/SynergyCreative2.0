package net.synergyserver.synergycreative.commands;

import net.synergyserver.synergycore.commands.CommandDeclaration;
import net.synergyserver.synergycore.commands.CommandFlags;
import net.synergyserver.synergycore.commands.MainCommand;
import net.synergyserver.synergycore.commands.SenderType;
import net.synergyserver.synergycore.configs.Message;
import net.synergyserver.synergycore.utils.MathUtil;
import org.bukkit.Material;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

@CommandDeclaration(
        commandName = "item",
        aliases = {"i" , "giveme"},
        permission = "syn.item",
        usage = "/item <Name> <Amount>",
        description = "Gives the player a specified item",
        maxArgs = 2,
        minArgs = 1,
        validSenders = SenderType.PLAYER
)

public class ItemCommand extends MainCommand {
    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        Player player = (Player) sender;

        //Likely going to use better method than this for matching material.
        Material type = Material.matchMaterial(args[0]);

        if (type == null) {
            player.sendMessage(Message.format("commands.item.error.no_match", args[0]));
            return false;
        }

        //Default amount to 1 if no amount is supplied
        int amount = 1;

        if (args.length == 2) {
            if (!MathUtil.isPositiveInteger(args[1])) {
                player.sendMessage(Message.format("commands.item.error.not_an_integer", args[1]));
                return false;
            }
            amount = Integer.parseInt(args[1]);
        }

        player.getInventory().addItem(new ItemStack(type,amount));

        player.sendMessage(Message.format("commands.item.info.enjoy_items", type.toString()
                .toLowerCase().replace("_"," ")));
        return true;
    }
}