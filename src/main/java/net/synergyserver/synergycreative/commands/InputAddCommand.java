package net.synergyserver.synergycreative.commands;

import net.synergyserver.synergycore.commands.CommandDeclaration;
import net.synergyserver.synergycore.commands.CommandFlags;
import net.synergyserver.synergycore.commands.SenderType;
import net.synergyserver.synergycore.commands.SubCommand;
import org.bukkit.command.CommandSender;

@CommandDeclaration(
        commandName = "add",
        aliases = {"create", "set", "make"},
        permission = "syn.input.add",
        usage = "/input add <cooldown|autoactivation|wireless>",
        description = "Adds the given functionality to the input you are looking at.",
        minArgs = 1,
        validSenders = SenderType.PLAYER,
        parentCommandName = "input"
)
public class InputAddCommand extends SubCommand {

    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        return false;
    }
}
