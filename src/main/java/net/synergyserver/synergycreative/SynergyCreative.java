package net.synergyserver.synergycreative;

import net.synergyserver.synergycore.SynergyCore;
import net.synergyserver.synergycore.SynergyPlugin;
import net.synergyserver.synergycore.commands.CommandManager;
import net.synergyserver.synergycore.configs.Message;
import net.synergyserver.synergycore.configs.PluginConfig;
import net.synergyserver.synergycore.database.MongoDB;
import net.synergyserver.synergycore.settings.SettingManager;
import net.synergyserver.synergycreative.commands.EnchantCommand;
import net.synergyserver.synergycreative.commands.FireworkClearCommand;
import net.synergyserver.synergycreative.commands.FireworkCommand;
import net.synergyserver.synergycreative.commands.FireworkLaunchCommand;
import net.synergyserver.synergycreative.commands.FireworkMetaCommand;
import net.synergyserver.synergycreative.commands.FireworkPowerCommand;
import net.synergyserver.synergycreative.commands.InputAddAutoActivationCommand;
import net.synergyserver.synergycreative.commands.InputAddCommand;
import net.synergyserver.synergycreative.commands.InputAddCooldownCommand;
import net.synergyserver.synergycreative.commands.InputAddWirelessCommand;
import net.synergyserver.synergycreative.commands.InputCommand;
import net.synergyserver.synergycreative.commands.InputInfoCommand;
import net.synergyserver.synergycreative.commands.InputRemoveCommand;
import net.synergyserver.synergycreative.commands.ItemCommand;
import net.synergyserver.synergycreative.commands.NameCommand;
import net.synergyserver.synergycreative.commands.RepairCommand;
import net.synergyserver.synergycreative.commands.SignalCommand;
import net.synergyserver.synergycreative.commands.WorkbenchCommand;
import net.synergyserver.synergycreative.inputs.AutoActivationInputFunction;
import net.synergyserver.synergycreative.inputs.CooldownInputFunction;
import net.synergyserver.synergycreative.inputs.Input;
import net.synergyserver.synergycreative.inputs.InputManager;
import net.synergyserver.synergycreative.inputs.WirelessInputFunction;
import net.synergyserver.synergycreative.listeners.PlayerListener;
import net.synergyserver.synergycreative.listeners.PlayerMoveListener;
import net.synergyserver.synergycreative.listeners.PlayerPlotListener;
import net.synergyserver.synergycreative.listeners.SynergyListener;
import net.synergyserver.synergycreative.listeners.WorldListener;
import net.synergyserver.synergycreative.settings.CreativeToggleSetting;
import org.bukkit.Bukkit;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.List;

/**
 * The main class for SynergyCreative.
 */
public class SynergyCreative extends JavaPlugin implements SynergyPlugin {

    @Override
    public void onEnable() {
        getLogger().info("Hooking into SynergyCore...");
        SynergyCore.getPlugin().registerSynergyPlugin(this);
    }

    public void postLoad() {
        getLogger().info("Caching inputs and checking data...");
        InputManager im = InputManager.getInstance();
        getLogger().info("Removed " + im.cacheAndCleanse() + " corrupted inputs.");

        getLogger().info("Hooking into WorldEdit...");
        // WorldEdit.getInstance().getEventBus().register(new WorldEditListener());
        // FaweAPI.addMaskManager(InputMaskManager.getInstance());;
    }

    @Override
    public void onDisable() {
        getLogger().info("Saving plugin configuration...");
        PluginConfig.getInstance().save(this);

        getLogger().info("Saving messages...");
        Message.getInstance().save(this);
    }

    public void registerCommands() {
        CommandManager cm = CommandManager.getInstance();

        cm.registerMainCommand(this, NameCommand.class);
        cm.registerMainCommand(this, SignalCommand.class);

        cm.registerMainCommand(this, WorkbenchCommand.class);
        cm.registerMainCommand(this, EnchantCommand.class);

        cm.registerMainCommand(this, InputCommand.class);
        cm.registerSubCommand(this, InputAddCommand.class);
        cm.registerSubCommand(this, InputAddCooldownCommand.class);
        cm.registerSubCommand(this, InputAddAutoActivationCommand.class);
        cm.registerSubCommand(this, InputAddWirelessCommand.class);
        cm.registerSubCommand(this, InputRemoveCommand.class);
        cm.registerSubCommand(this, InputInfoCommand.class);

        cm.registerMainCommand(this, FireworkCommand.class);
        cm.registerSubCommand(this, FireworkMetaCommand.class);
        cm.registerSubCommand(this, FireworkLaunchCommand.class);
        cm.registerSubCommand(this, FireworkClearCommand.class);
        cm.registerSubCommand(this, FireworkPowerCommand.class);

        cm.registerMainCommand(this, ItemCommand.class);
        cm.registerMainCommand(this, RepairCommand.class);

    }

    public void registerSettings() {
        SettingManager sm = SettingManager.getInstance();
        sm.registerSettings(CreativeToggleSetting.class);
    }

    public void registerListeners() {
        PluginManager pm = Bukkit.getPluginManager();
        pm.registerEvents(new SynergyListener(), this);
        pm.registerEvents(new PlayerListener(), this);
        pm.registerEvents(new PlayerMoveListener(), this);
        pm.registerEvents(new PlayerPlotListener(), this);
        pm.registerEvents(new WorldListener(), this);
    }

    public void mapClasses() {
        MongoDB db = MongoDB.getInstance();
        db.mapClasses(
                CreativeWorldGroupProfile.class,
                Input.class,
                AutoActivationInputFunction.class,
                CooldownInputFunction.class,
                WirelessInputFunction.class
        );
    }

    /**
     * Convenience method for getting this plugin.
     *
     * @return The object representing this plugin.
     */
    public static SynergyCreative getPlugin() {
        return (SynergyCreative) getProvidingPlugin(SynergyCreative.class);
    }

    public String[] getAliases() {
        return new String[]{getName(), "SynCreative", "Creative", "SynCR", "CR"};
    }

    /**
     * Gets the list of worlds that this plugin handles.
     *
     * @return The world group of this plugin.
     */
    public static List<String> getWorldGroup() {
        return PluginConfig.getConfig(getPlugin()).getStringList("world_group.worlds");
    }

    /**
     * Gets the name of the world group that this plugin handles.
     *
     * @return The world group name of this plugin.
     */
    public static String getWorldGroupName() {
        return PluginConfig.getConfig(getPlugin()).getString("world_group.name");
    }
}